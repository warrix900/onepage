package yang.com.onepage.Fragment;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.SimpleAdapter;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import androidx.navigation.Navigation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import yang.com.onepage.DataBundle.TransType;
import yang.com.onepage.R;

public class FunctionFragment extends Fragment implements AdapterView.OnItemClickListener{
    final String kRes = "itemImageView";
    final String kText = "itemName";
    Handler handler;
    Runnable home;
    boolean isUsing;

    @SuppressWarnings({"MismatchedQueryAndUpdateOfCollection"})
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,@Nullable ViewGroup container,@Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frame_func,container,false);
        Log.d("func","create 一次");
        //圖檔
        int[] imgRes = {R.drawable.sale,R.drawable.refund,R.drawable.resource_void,
                        R.drawable.settle,R.drawable.report,R.drawable.reprint};
        //標題
        String[] itemName = {"銷售","退貨","取消","結帳","查詢","重印"};
        GridView mGridView = (GridView) view.findViewById(R.id.grid_func);
        List<HashMap<String, java.io.Serializable>> data = new ArrayList<>();
        int len = itemName.length;
        for (int i=0;i<len;i++){
            HashMap<String, java.io.Serializable> map = new HashMap<>();
            map.put(kRes,imgRes[i]);
            map.put(kText,itemName[i]);
            data.add(map);
        }

        SimpleAdapter mAdapter = new SimpleAdapter(getActivity(),data,R.layout.item_idle,
                new String[]{kRes,kText},new int[]{R.id.img_item,R.id.tv_item});
        mGridView.setAdapter(mAdapter);
        mGridView.setOnItemClickListener(this);
        handler = new Handler();
        home = () -> {
            if(isUsing){
                isUsing = false;
                return;
            }
            Navigation.findNavController(view).navigate(R.id.action_functionFragment_to_idleFragment);//跳轉
        };
        handler.postDelayed(home,5000);
        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> parent,View view,int position,long id) {
        Log.d("Func","點事件" + position);
        isUsing = true; //阻塞待機操作事件
        FunctionFragmentDirections.ActionFunctionFragmentToInputFragment action =
                FunctionFragmentDirections.actionFunctionFragmentToInputFragment();
        switch (position){
            case 0:
                action.setStringArgsTransType(TransType.Sale);
                Navigation.findNavController(view).navigate(action);//跳轉
                break;
            case 1:
                action.setStringArgsTransType(TransType.Refund);
                Navigation.findNavController(view).navigate(action);//跳轉
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            case 5:
                break;
            default:
                //跳unknown類型
                Log.d("Func","未知類型功能: " + position);
                break;
        }
//        view.setBackgroundResource(R.drawable.button_selected);
    }

    @Override
    public void onDestroyView(){
        Log.d("Func","解構回收事件");
        if(handler != null){
            handler.removeCallbacks(home);
            Log.d("Func","回收觸發事件");
        }
        super.onDestroyView();
    }

}
