package yang.com.onepage.Fragment;


import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import yang.com.onepage.AlertDialog.ReaderDialog;
import yang.com.onepage.DataBundle.TransType;
import yang.com.onepage.MainActivity;
import yang.com.onepage.Custom.MyKeyboard;
import yang.com.onepage.R;
import yang.com.onepage.Util.TextInputHelper;
import yang.com.onepage.Util.UiUtil;

public class InputFragment extends Fragment implements MainActivity.TextError{

//    private int position = 0;

//    public InputFragment(int position){
//        this.position = position;
//    }

    private final List<TextInputHelper> helpers = new ArrayList<>();
    final UiUtil util= new UiUtil();
    Handler handler;

//    TableRow tab_amt,tab_store,tab_card;
//    TextView ;
    EditText et_amt,et_store,et_card,et_expire,et_auth;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,@Nullable ViewGroup container,@Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frame_input,container,false);
        handler =new Handler();
        TransType actionCode = TransType.NA;
        if (getArguments() != null) {
            actionCode = InputFragmentArgs.fromBundle(getArguments()).getStringArgsTransType();
        }
        initUI(view,actionCode);
        return view;
    }

    private void initUI(View view,@NotNull TransType actionCode){
        switch (actionCode){
            case NA:
                Log.d("N/A","Unknown TransType");
                break;
            case Sale:
                TextView title_sale     = view.findViewById(R.id.title);
                Button bt_readCard_sale = view.findViewById(R.id.bt_readCard);
                Button bt_confirm_sale  = view.findViewById(R.id.bt_confirm);
                Button bt_shift_sale    = view.findViewById(R.id.bt_shift);

                TableRow tab_amt_sale   = view.findViewById(R.id.un_tab1);
                TextView tile_amt_sale  = view.findViewById(R.id.un_title1);
                et_amt                  = view.findViewById(R.id.un_ed1);

                TableRow tab_store_sale = view.findViewById(R.id.un_tab2);
                TextView tile_store_sale= view.findViewById(R.id.un_title2);
                et_store                = view.findViewById(R.id.un_ed2);

                MyKeyboard keyboard_sale= view.findViewById(R.id.keyboard);

                addHelper(bt_confirm_sale,1,et_amt);
                bt_shift_sale.setOnClickListener(v -> keyboard_sale.next());
                bt_readCard_sale.setOnClickListener(v -> handler.post(() -> {
                    ReaderDialog dialog = new ReaderDialog();
                    dialog.setmText("請插卡/刷卡/感應");
                    mshow(dialog);
                }));

                handler.post(() -> {
                    title_sale.setText("銷售交易");
                    util.openTab(tab_amt_sale,tab_store_sale);
                    util.unKnownSetter(et_amt,tile_amt_sale,"金額","請輸入金額",9,InputType.TYPE_CLASS_NUMBER);
                    util.unKnownSetter(et_store,tile_store_sale,"櫃號","請輸入櫃號",10,InputType.TYPE_CLASS_TEXT);//櫃號其實要看TMS

                    util.openKeyBoard(getActivity(),et_amt,keyboard_sale);//這個是最後設定取得焦點的
                    util.openKeyBoard(getActivity(),et_store,keyboard_sale);
                    et_store.setImeOptions(EditorInfo.IME_ACTION_NONE);

                    et_amt.requestFocus();
                });
                break;
            case Refund:
                TextView title      = view.findViewById(R.id.title);
                Button bt_readCard  = view.findViewById(R.id.bt_readCard);
                Button bt_confirm   = view.findViewById(R.id.bt_confirm);
                Button bt_shift     = view.findViewById(R.id.bt_shift);

                TableRow tab_amt    = view.findViewById(R.id.un_tab1);
                TextView tile_amt   = view.findViewById(R.id.un_title1);
                et_amt              = view.findViewById(R.id.un_ed1);

                TableRow tab_store  = view.findViewById(R.id.un_tab2);
                TextView tile_store = view.findViewById(R.id.un_title2);
                et_store            = view.findViewById(R.id.un_ed2);

                TableRow tab_card   = view.findViewById(R.id.un_tab3);
                TextView tile_card  = view.findViewById(R.id.un_title3);
                et_card             = view.findViewById(R.id.un_ed3);

                TableRow tab_exp    = view.findViewById(R.id.un_tab4);
                TextView tile_exp   = view.findViewById(R.id.un_title4);
                et_expire           = view.findViewById(R.id.un_ed4);

                TableRow tab_auth   = view.findViewById(R.id.un_tab5);
                TextView tile_auth  = view.findViewById(R.id.un_title5);
                et_auth             = view.findViewById(R.id.un_ed5);


                MyKeyboard keyboard = view.findViewById(R.id.keyboard);

                addHelper(bt_confirm,1,et_amt);
                addHelper(bt_confirm,14,et_card);//卡號最小14
                addHelper(bt_confirm,4,et_expire);//有效期最小4位
                addHelper(bt_confirm,6,et_auth);//授權碼最低6位
                bt_shift.setOnClickListener(v -> keyboard.next());
                bt_readCard.setOnClickListener(v -> handler.post(() -> {
                    ReaderDialog dialog = new ReaderDialog();
                    dialog.setmText("請刷卡/感應");
                    mshow(dialog);
                }));

                handler.post(() -> {
                    title.setText("退貨交易");
                    util.openTab(tab_amt,tab_auth,tab_store,tab_card,tab_exp);
                    util.unKnownSetter(et_amt,tile_amt,"金額","請輸入金額",9,InputType.TYPE_CLASS_NUMBER);
                    util.unKnownSetter(et_store,tile_store,"櫃號","請輸入櫃號",10,InputType.TYPE_CLASS_TEXT);//櫃號其實要看TMS
                    util.unKnownSetter(et_card,tile_card,"卡號","請輸入卡號",22,InputType.TYPE_CLASS_NUMBER);
                    util.unKnownSetter(et_expire,tile_exp,"有效期","請輸入有效期",5,InputType.TYPE_CLASS_TEXT);
                    util.unKnownSetter(et_auth,tile_auth,"授權碼","請輸入授權碼",6,InputType.TYPE_CLASS_TEXT);//其實有限範圍,之後再調

                    util.openKeyBoard(getActivity(),et_amt,keyboard);//這個是最後設定取得焦點的
                    util.openKeyBoard(getActivity(),et_store,keyboard);
                    util.openKeyBoard(getActivity(),et_card,keyboard);
                    util.openKeyBoard(getActivity(),et_expire,keyboard);
                    util.openKeyBoard(getActivity(),et_auth,keyboard);
                    et_auth.setImeOptions(EditorInfo.IME_ACTION_NONE);

                    et_amt.requestFocus();
                });

                break;
            case Redeem:
            case Redeem_Refund:
            case Install:
            case Install_Refund:
            case Preauth:
            case PreauthComplete:
            case Force:
        }
    }
    private void addHelper(Button bt_confirm,int minLen,EditText... editText){
        TextInputHelper helper = new TextInputHelper(bt_confirm,minLen,this);
        //可添加一个或者多个EditText
        helper.addViews(editText);
        helpers.add(helper);
    }

    private void setErrorEditText(@NotNull EditText editText,String errMsg) {
        //設置錯誤觸發時會出現的Icon*/
//        Drawable drawable = getResources().getDrawable(R.drawable.alert2);
//        Drawable drawable = ResourcesCompat.getDrawable(,R.drawable.alert2,null);
//        drawable.setBounds(0, 0, 70, 70);
        //設置Error時出現的錯誤訊息提示與ICON*/
//        editText.setError(errMsg, drawable);
        editText.setError(errMsg);
    }


    @Override
    public void onDestroy(){
        for (TextInputHelper helper: helpers){
            helper.removeViews();
        }
        super.onDestroy();
    }

    @Override
    public void onTextError(EditText editText,String errMsg) {
        setErrorEditText(editText,errMsg);
    }

    private void navigate(){
    }
    private void mshow(DialogFragment dialog){
        if(this.getParentFragmentManager() == null){ return;}
        dialog.show(this.getParentFragmentManager(),"dialogFg");
        dialog.setCancelable(false);
    }
}
