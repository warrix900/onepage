package yang.com.onepage;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;


public class MainActivity extends AppCompatActivity {

//    private ViewPager2 mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        mViewPager = this.findViewById(R.id.pager_main);
//        List<Fragment> FgList = new ArrayList<>();
//        FgList.add(new IdleFragment());
//        FgList.add(new InputFragment(0));
//        FgList.add(new InputFragment(1));
//        FgList.add(new InputFragment(2));
//        MainPageAdapter mainPageAdapter = new MainPageAdapter(this,FgList);
//        mViewPager.setAdapter(mainPageAdapter);

    }

    public interface TextError{
        void onTextError(EditText editText,String errMsg);
    }
}