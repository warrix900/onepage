package yang.com.onepage.AlertDialog;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;

import yang.com.onepage.Custom.CustomView;
import yang.com.onepage.R;

public class ReaderDialog extends DialogFragment {
    String mText;
    TextView tvhint;
    Button button_cancel, button_manual;
    ProgressBar progress;
    CustomView customView;
    Handler handler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.Mdialog);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
        /*設定自定義介面*/
        View view = inflater.inflate(R.layout.dialog_progress, container);
        handler = new Handler();
        initView(view);
        initTimeout();
        return view;
    }

    private void initView(View v) {
        button_cancel = v.findViewById(R.id.button_cancel);
        button_manual = v.findViewById(R.id.button_manual);
        tvhint = v.findViewById(R.id.tv_hint);
        progress = v.findViewById(R.id.load_progressBar);
        customView = v.findViewById(R.id.custom_view);
        success(false);//剛開啟視窗還在轉圈
        setText(mText);
        button_cancel.setOnClickListener(v1 -> exit());
//        button_manual.setOnClickListener(v1 -> );

    }

    private void initTimeout() {
        handler.postDelayed(this::exit,150*1000);
    }


    public void success(boolean ready){
        if(ready){
            new Handler().postDelayed(() -> {
                progress.setVisibility(View.GONE);
                customView.setVisibility(View.VISIBLE);
                customView.circleAnimation();
            },0);//立刻執行UI轉換

        }else {
            //尚未有需要在流程中轉換UI
            progress.setVisibility(View.VISIBLE);
            customView.setVisibility(View.GONE);
        }
    }
    public void setmText(String text){
        mText = text;
    }

    public void setText(String msg){
        if(tvhint == null){return;}
        tvhint.setText(msg);
        if(tvhint.length() >= 8){
            tvhint.setTextSize(16);//超過八個字會斷行要改字大小
        }
    }

    public void exit(){
        dismissAllowingStateLoss();
    }

    //FIXME 留下影響介面,能將讀卡結果通知外面
}
