package yang.com.onepage.DataBundle;

public enum TransType {
    NA,
    Sale,Void_Sale,
    Refund,Void_Refund,
    Redeem,Void_Redeem,
    Redeem_Refund,Void_Redeem_Refund,
    Install,Void_Install,
    Install_Refund,Void_Install_Refund,
    Preauth,Void_Preauth,
    PreauthComplete,Void_PreauthComplete,
    Force,CALLBANK,
    Tip,Adjust,Tc,

}
