package yang.com.onepage.Fragment;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import androidx.navigation.Navigation;

import yang.com.onepage.R;

public class IdleFragment extends Fragment {
    boolean isBusy;
    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,@Nullable ViewGroup container,@Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frame_idle,container,false);
        view.setOnTouchListener((v,event) -> {
            if(event.getAction() == MotionEvent.ACTION_BUTTON_PRESS || event.getAction() == MotionEvent.ACTION_MOVE){
                if(!isBusy){
                    Navigation.findNavController(v).navigate(R.id.action_idleFragment_to_functionFragment);//跳轉
                } else {
                    Log.d("Idle","Idle Busy Now ,ignore user touch event");
                }
            }
            return true;
        });
//        ImageView iLogo = view.findViewById(R.id.img_logo);
//        ImageView iLabel = view.findViewById(R.id.img_label);
//        TextView  textView = view.findViewById(R.id.text_idle);
        return view;
    }

    //待機補送電簽
    private void esc_retry(){}
    //待機排程
    private void schedual(){}

}
