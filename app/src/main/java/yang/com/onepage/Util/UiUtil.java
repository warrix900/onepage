package yang.com.onepage.Util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.EditText;
import android.widget.TableRow;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import yang.com.onepage.Custom.MyKeyboard;
import yang.com.onepage.R;

public class UiUtil {
    //畫面文字框使用工具
    public void setTitle(@NotNull TextView textView,String text){
        textView.setText(text);
    }
    public void setHint(@NotNull EditText editText,String text){
        editText.setHint(text);
    }
    public void setMaxLen(@NotNull EditText editText,int len){
        editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(len)});
    }
    public void setInputType(@NotNull EditText editText,int InputType){
        editText.setInputType(InputType);
    }
    public void unKnownSetter(@NotNull EditText editText ,@NotNull TextView textView,String title,String hint,int maxLen,int inputType){
        setTitle(textView,title);
        setHint(editText,hint);
        setMaxLen(editText,maxLen);
        setInputType(editText,inputType);
    }
    public void openTab(TableRow @NotNull ... tab){
        for (TableRow table:tab){
            table.setVisibility(View.VISIBLE);
        }
    }
    public void closeTab(TableRow @NotNull ... tab){
        for (TableRow table: tab){
            table.setVisibility(View.GONE);
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    public void openKeyBoard(Context context,EditText editText,@NotNull MyKeyboard keyboard){
        if(context == null){return;}
        Animation animation = AnimationUtils.loadAnimation(context,R.anim.slide_in_bottom);
        LayoutAnimationController controller = new LayoutAnimationController(animation);
        controller.setOrder(LayoutAnimationController.ORDER_REVERSE);
        controller.setDelay(.2f);
        keyboard.setVisibility(View.VISIBLE);
        keyboard.startAnimation(animation);

        //覆蓋原本點擊事件
        editText.setOnTouchListener((v,event) -> {
            // pass the InputConnection from the EditText to the keyboard
            InputConnection ic = editText.onCreateInputConnection(new EditorInfo());
            keyboard.setInputConnection(ic);
            editText.setBackgroundResource(R.drawable.shape_focus);
            editText.requestFocus();
            return true;
        });
        editText.setOnFocusChangeListener((v,focus) -> {
            if(!focus){
                editText.setBackgroundResource(R.drawable.shape_normal);
            } else {
                setKeyboardType(editText,keyboard);
                // pass the InputConnection from the EditText to the keyboard
                InputConnection ic = editText.onCreateInputConnection(new EditorInfo());
                keyboard.setInputConnection(ic);
                editText.setBackgroundResource(R.drawable.shape_focus);
            }

        });

    }
    public void closeKeyBoard(@NotNull MyKeyboard view){
        view.setVisibility(View.GONE);
        view.setInputConnection(null);
    }

    public void setKeyboardType(@NotNull EditText editText,MyKeyboard keyboard){
        int type = editText.getInputType();
        Log.d("輸入頁面","預先設置鍵盤設定" + type);
        if (type == InputType.TYPE_CLASS_NUMBER) {
            keyboard.changeKeyBoard(MyKeyboard.Type_num);
        } else {
            keyboard.changeKeyBoard(MyKeyboard.Type_en1);
        }
    }

}
