package yang.com.onepage.Custom;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import yang.com.onepage.R;

public class MyKeyboard extends LinearLayout implements View.OnClickListener {
    public MyKeyboard(Context context) {
        super(context);
    }

    public MyKeyboard(Context context,@Nullable AttributeSet attrs) {
        super(context,attrs);
        init(context, attrs);
    }

    public MyKeyboard(Context context,@Nullable AttributeSet attrs,int defStyleAttr) {
        super(context,attrs,defStyleAttr);
        init(context, attrs);
    }

    public MyKeyboard(Context context,AttributeSet attrs,int defStyleAttr,int defStyleRes) {
        super(context,attrs,defStyleAttr,defStyleRes);
        init(context, attrs);
    }

    LinearLayout layout_num,layout_text1,layout_text2;
    public boolean isCaps;

    // This will map the button resource id to the String value that we want to
    // input when that button is clicked.
    SparseArray<String> keyValues = new SparseArray<>();

    // Our communication link to the EditText
    InputConnection inputConnection;
    Button mButtona,mButtonb,mButtonc,mButtond,mButtone,mButtonf,mButtong,
            mButtonh,mButtoni,mButtonj,mButtonk,mButtonl,mButtonm,mButtonn,
            mButtono,mButtonp,mButtonq,mButtonr,mButtons,mButtont,mButtonu,
            mButtonv,mButtonw,mButtonx,mButtony,mButtonz;
    private void init(Context context, AttributeSet attrs) {

        // initialize buttons
        LayoutInflater.from(context).inflate(R.layout.keyboard, this, true);
        // keyboard keys (buttons)
        Button mButton1 = (Button) findViewById(R.id.button_1);
        Button mButton2 = (Button) findViewById(R.id.button_2);
        Button mButton3 = (Button) findViewById(R.id.button_3);
        Button mButton4 = (Button) findViewById(R.id.button_4);
        Button mButton5 = (Button) findViewById(R.id.button_5);
        Button mButton6 = (Button) findViewById(R.id.button_6);
        Button mButton7 = (Button) findViewById(R.id.button_7);
        Button mButton8 = (Button) findViewById(R.id.button_8);
        Button mButton9 = (Button) findViewById(R.id.button_9);
        Button mButton0 = (Button) findViewById(R.id.button_0);
        Button mButtonDelete = (Button) findViewById(R.id.button_delete);
        Button mButtonEnter = (Button) findViewById(R.id.button_enter);

        mButtona = (Button) findViewById(R.id.button_a);
        mButtonb = (Button) findViewById(R.id.button_b);
        mButtonc = (Button) findViewById(R.id.button_c);
        mButtond = (Button) findViewById(R.id.button_d);
        mButtone = (Button) findViewById(R.id.button_e);
        mButtonf = (Button) findViewById(R.id.button_f);
        mButtong = (Button) findViewById(R.id.button_g);
        mButtonh = (Button) findViewById(R.id.button_h);
        mButtoni = (Button) findViewById(R.id.button_i);
        mButtonj = (Button) findViewById(R.id.button_j);
        mButtonk = (Button) findViewById(R.id.button_k);
        mButtonl = (Button) findViewById(R.id.button_l);
        mButtonm = (Button) findViewById(R.id.button_m);
        mButtonn = (Button) findViewById(R.id.button_n);
        mButtono = (Button) findViewById(R.id.button_o);
        mButtonp = (Button) findViewById(R.id.button_p);
        mButtonq = (Button) findViewById(R.id.button_q);
        mButtonr = (Button) findViewById(R.id.button_r);
        mButtons = (Button) findViewById(R.id.button_s);
        mButtont = (Button) findViewById(R.id.button_t);
        mButtonu = (Button) findViewById(R.id.button_u);
        mButtonv = (Button) findViewById(R.id.button_v);
        mButtonw = (Button) findViewById(R.id.button_w);
        mButtonx = (Button) findViewById(R.id.button_x);
        mButtony = (Button) findViewById(R.id.button_y);
        mButtonz = (Button) findViewById(R.id.button_z);
        Button mButtonDt= (Button) findViewById(R.id.button_dot);
        Button mButtonSh= (Button) findViewById(R.id.button_slash);
        Button mButtonCp= (Button) findViewById(R.id.button_cap);
        Button mButtonEnter2 = (Button) findViewById(R.id.button_enter_2);
        Button mButtonEnter3 = (Button) findViewById(R.id.button_enter_3);

        layout_num = (LinearLayout) findViewById(R.id.keyboard_num);
        layout_text1 = (LinearLayout) findViewById(R.id.keyboard_text1);
        layout_text2 = (LinearLayout) findViewById(R.id.keyboard_text2);

        // set button click listeners
        mButton1.setOnClickListener(this);
        mButton2.setOnClickListener(this);
        mButton3.setOnClickListener(this);
        mButton4.setOnClickListener(this);
        mButton5.setOnClickListener(this);
        mButton6.setOnClickListener(this);
        mButton7.setOnClickListener(this);
        mButton8.setOnClickListener(this);
        mButton9.setOnClickListener(this);
        mButton0.setOnClickListener(this);
        mButtonDelete.setOnClickListener(this);
        mButtonEnter.setOnClickListener(this);
        mButtona.setOnClickListener(this);
        mButtonb.setOnClickListener(this);
        mButtonc.setOnClickListener(this);
        mButtond.setOnClickListener(this);
        mButtone.setOnClickListener(this);
        mButtonf.setOnClickListener(this);
        mButtong.setOnClickListener(this);
        mButtonh.setOnClickListener(this);
        mButtoni.setOnClickListener(this);
        mButtonj.setOnClickListener(this);
        mButtonk.setOnClickListener(this);
        mButtonl.setOnClickListener(this);
        mButtonm.setOnClickListener(this);
        mButtonn.setOnClickListener(this);
        mButtono.setOnClickListener(this);
        mButtonp.setOnClickListener(this);
        mButtonq.setOnClickListener(this);
        mButtonr.setOnClickListener(this);
        mButtons.setOnClickListener(this);
        mButtont.setOnClickListener(this);
        mButtonu.setOnClickListener(this);
        mButtonv.setOnClickListener(this);
        mButtonw.setOnClickListener(this);
        mButtonx.setOnClickListener(this);
        mButtony.setOnClickListener(this);
        mButtonz.setOnClickListener(this);
        mButtonDt.setOnClickListener(this);
        mButtonSh.setOnClickListener(this);
        mButtonCp.setOnClickListener(this);
        mButtonEnter2.setOnClickListener(this);
        mButtonEnter3.setOnClickListener(this);


        // map buttons IDs to input strings
        keyValues.put(R.id.button_1, "1");
        keyValues.put(R.id.button_2, "2");
        keyValues.put(R.id.button_3, "3");
        keyValues.put(R.id.button_4, "4");
        keyValues.put(R.id.button_5, "5");
        keyValues.put(R.id.button_6, "6");
        keyValues.put(R.id.button_7, "7");
        keyValues.put(R.id.button_8, "8");
        keyValues.put(R.id.button_9, "9");
        keyValues.put(R.id.button_0, "0");
        keyValues.put(R.id.button_enter, "\n");
        keyValues.put(R.id.button_a, "a");
        keyValues.put(R.id.button_b, "b");
        keyValues.put(R.id.button_c, "c");
        keyValues.put(R.id.button_d, "d");
        keyValues.put(R.id.button_e, "e");
        keyValues.put(R.id.button_f, "f");
        keyValues.put(R.id.button_g, "g");
        keyValues.put(R.id.button_h, "h");
        keyValues.put(R.id.button_i, "i");
        keyValues.put(R.id.button_j, "j");
        keyValues.put(R.id.button_k, "k");
        keyValues.put(R.id.button_l, "l");
        keyValues.put(R.id.button_m, "m");
        keyValues.put(R.id.button_n, "n");
        keyValues.put(R.id.button_o, "o");
        keyValues.put(R.id.button_p, "p");
        keyValues.put(R.id.button_q, "q");
        keyValues.put(R.id.button_r, "r");
        keyValues.put(R.id.button_s, "s");
        keyValues.put(R.id.button_t, "t");
        keyValues.put(R.id.button_u, "u");
        keyValues.put(R.id.button_v, "v");
        keyValues.put(R.id.button_w, "w");
        keyValues.put(R.id.button_x, "x");
        keyValues.put(R.id.button_y, "y");
        keyValues.put(R.id.button_z, "z");
        keyValues.put(R.id.button_slash, "/");
        keyValues.put(R.id.button_dot, ".");
    }

    @Override
    public void onClick(View v) {

        // do nothing if the InputConnection has not been set yet
        if (inputConnection == null) return;

        // Delete text or input key value
        // All communication goes through the InputConnection
        if (v.getId() == R.id.button_delete) {
            CharSequence selectedText = inputConnection.getSelectedText(0);
            if (TextUtils.isEmpty(selectedText)) {
                // no selection, so delete previous character
                inputConnection.deleteSurroundingText(1, 0);
            } else {
                // delete the selection
                inputConnection.commitText("", 1);
            }
        }else if (v.getId() == R.id.button_enter || v.getId() == R.id.button_enter_2
                || v.getId() == R.id.button_enter_3){
            inputConnection.performEditorAction(EditorInfo.IME_ACTION_GO);

        }else if(v.getId() == R.id.button_cap){
            isCaps = !isCaps;
            setCaps(isCaps,mButtona,mButtonb,mButtonc,mButtond,mButtone,mButtonf,
                    mButtong,mButtonh,mButtoni,mButtonj,mButtonk,mButtonl,mButtonm,
                    mButtonn,mButtono,mButtonp,mButtonq,mButtonr,mButtons,mButtont,
                    mButtonu,mButtonv,mButtonw,mButtonx,mButtony,mButtonz);
        } else {
            String value = keyValues.get(v.getId());
            if(isCaps){value = value.toUpperCase();}
            inputConnection.commitText(value, 1);
        }
    }

    // The activity (or some parent or controller) must give us
    // a reference to the current EditText's InputConnection
    public void setInputConnection(InputConnection ic) {
        this.inputConnection = ic;
    }

    public static final int Type_num = 30;
    public static final int Type_en1 = 40;
    public static final int Type_en2 = 50;
    public int Type_now;

    public void changeKeyBoard(int Type){
        Type_now = Type;
        switch (Type){
            case Type_num:
                layout_num.setVisibility(VISIBLE);
                layout_text1.setVisibility(GONE);
                layout_text2.setVisibility(GONE);
                break;
            case Type_en1:
                layout_num.setVisibility(GONE);
                layout_text1.setVisibility(VISIBLE);
                layout_text2.setVisibility(GONE);
                break;
            case Type_en2:
                layout_num.setVisibility(GONE);
                layout_text1.setVisibility(GONE);
                layout_text2.setVisibility(VISIBLE);
                break;
            default:
                break;
        }
    }
    public void next(){
        switch (Type_now){
            case Type_num:
                changeKeyBoard(Type_en1);
                break;
            case Type_en1:
                changeKeyBoard(Type_en2);
                break;
            case 0:
            case Type_en2:
                changeKeyBoard(Type_num);
                break;
        }
    }
    public void setCaps(boolean isCaps,Button... button){
        for(Button bt : button){
            if (isCaps){
                bt.setText(bt.getText().toString().toUpperCase());
            } else {
                bt.setText(bt.getText().toString().toLowerCase());
            }
        }
    }

}
